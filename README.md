# Lunni

A simple personal PaaS / hosting, based on Docker Swarm, Traefik, and Portainer.


## Setup

Just run the installer and follow the instructions:

```sh
wget https://lunni.ale.sh/setup.sh  # grab the script
less setup.sh                       # inspect it
bash setup.sh                       # run it
```

After the setup, check out the [Quickstart](https://lunni.ale.sh/).


## See also

- [puffin](https://github.com/puffinrocks/puffin)
- [dockerswarm.rocks](https://dockerswarm.rocks/)
- <https://schemen.me/portainer-with-traefik/>
